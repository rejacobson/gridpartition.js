A grid-based spatial partition for javascript games.

In order to improve performance of games with moderate to high numbers of entities it becomes necessary to split the entities up based on their location in the world.

This library uses a grid-based partition and provides methods to add, remove and query entities in the grid.
