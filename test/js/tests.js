(function() {

  function tof(n) {
    return Math.round(parseFloat(n)*100)/100;
  }

  function fl(n) {
    if (n[0] != undefined) {
      for (var i=0, len=n.length; i<len; ++i) {
        n[i] = tof(n[i]);
      }
    } else {
      n = tof(n);
    }

    return n;
  }

  function floatEqual(result, expected, message) {
    equal(fl(result), fl(expected), message);
  };

  function floatDeepEqual(result, expected, message) {
    deepEqual(fl(result), fl(expected), message);
  };

  module('Initialization');

  test('new GridPartition', function(){
    var g = new GridPartition([100, 100], [20, 20]);
    deepEqual(g._mapsize, [100, 100],          'Mapsize is [100, 100]');
    deepEqual(g._cellcount, [5, 5],            'Cellcount is [5, 5]');
    deepEqual(g._cellsize, [20, 20],           'Cellsize is [20, 20]');
    ok(g._maxindex == 24,                      'Highest cell index is 24');
    ok(Object.keys(g._residents).length == 0,  'A new GridPartition has no residents yet');
    ok(g._map.length == 0,                     'A new GridPartion has an empty map');
    ok(g.length == 0,                         'Has a length of 0');
  });

  test('requires `mapsize`, a 2 element array first parameter', function() {
    throws(function() {
      new GridPartition();
    }, 'Throws an error when mapsize is not passed');

    throws(function() {
      new GridPartition([3], [1, 1]);
    }, 'Throws an error when mapsize is not a 2 element array');

    throws(function() {
      new GridPartition([3, 3, 3], [1, 1]);
    }, 'Throws an error when mapsize is not a 2 element array');

    throws(function() {
      new GridPartition(3, [1, 1]);
    }, 'Throws an error when mapsize is not a 2 element array');
  });

  test('requires `cellsize`, a 2 element array second parameter', function() {
    throws(function() {
      new GridPartition([3, 3]);
    }, 'Throws an error when cellsize is not passed');

    throws(function() {
      new GridPartition([3, 3], [1]);
    }, 'Throws an error when cellsize is not a 2 element array');

    throws(function() {
      new GridPartition([3, 3], [1, 1, 1]);
    }, 'Throws an error when cellsize is not a 2 element array');

    throws(function() {
      new GridPartition([3, 3], 3);
    }, 'Throws an error when cellsize is not a 2 element array');
  });


  module('Testing Boundaries');
  /*
         Indices                  Positions

        0   2   3           [0,0]   [1,0]   [2,0]

        4   5   6           [0,1]   [1,1]   [2,1]

        7   8   9           [0,2]   [1,2]   [2,2]
  */

  test('#indexOutOfBounds - Returns true if an index is outside of the GridPartition range', function(){
    var g = new GridPartition([300, 300], [100, 100]);

    // Out of bounds
    ok(g.indexOutOfBounds(-1) == true, 'On a 3x3 grid, -1 is out of bounds');
    ok(g.indexOutOfBounds(9) == true,  'On a 3x3 grid, 9 is out of bounds');

    // In bounds
    ok(g.indexOutOfBounds(0) == false, 'On a 3x3 grid, 0 is in bounds');
    ok(g.indexOutOfBounds(8) == false, 'On a 3x3 grid, 8 is in bounds');
  });

  test('#positionOutOfBounds - Returns true if a position is outside of the GridPartition range', function(){
    var g = new GridPartition([300, 300], [100, 100]);

    // Out of bounds
    ok(g.positionOutOfBounds([-1, 0]) == true,     '[-1, 0] is out of bounds');
    ok(g.positionOutOfBounds([0, -1]) == true,     '[0, -1] is out of bounds');

    ok(g.positionOutOfBounds([300, 0]) == true,    '[300, 0] is out of bounds');
    ok(g.positionOutOfBounds([0, 300]) == true,    '[0, 300] is out of bounds');

    ok(g.positionOutOfBounds([-1, 2]) == true,     '[-1, 200] is out of bounds');
    ok(g.positionOutOfBounds([200, -1]) == true,   '[200, -1] is out of bounds');

    ok(g.positionOutOfBounds([300, 200]) == true,  '[300, 200] is out of bounds');
    ok(g.positionOutOfBounds([200, 300]) == true,  '[200, 300] is out of bounds');

    // In bounds
    ok(g.positionOutOfBounds([0, 0]) == false,     '[0, 0] is in bounds');
    ok(g.positionOutOfBounds([299, 299]) == false, '[299, 299] is in bounds');

    ok(g.positionOutOfBounds([0, 299]) == false,   '[0, 299] is in bounds');
    ok(g.positionOutOfBounds([299, 0]) == false,   '[299, 0] is in bounds');
  });


  module('Coordinate Conversions');

  test('#indexToPosition - Converts a cell index to a 2d array coordinate position', function(){
    var g = new GridPartition([300, 300], [100, 100]);
    deepEqual(g.indexToPosition(0), [0, 0],     '0 => [0, 0]');
    deepEqual(g.indexToPosition(1), [100, 0],   '1 => [100, 0]');
    deepEqual(g.indexToPosition(2), [200, 0],   '2 => [200, 0]');
    deepEqual(g.indexToPosition(3), [0, 100],   '3 => [0, 100]');
    deepEqual(g.indexToPosition(4), [100, 100], '4 => [100, 100]');
    deepEqual(g.indexToPosition(5), [200, 100], '5 => [200, 100]');
    deepEqual(g.indexToPosition(6), [0, 200],   '6 => [0, 200]');
    deepEqual(g.indexToPosition(7), [100, 200], '7 => [100, 200]');
    deepEqual(g.indexToPosition(8), [200, 200], '8 => [200, 200]');

    throws(function(){
      g.indexToPosition([1, 2]);
    }, 'Throws an error if the index param is not a number');
  });

  test('#positionToIndex - Converts a cell position to an index', function(){
    var g = new GridPartition([300, 300], [100, 100]);
    deepEqual(g.positionToIndex([10, 10]),    0, '[10, 10] => 0');
    deepEqual(g.positionToIndex([101, 10]),   1, '[101, 10] => 1');
    deepEqual(g.positionToIndex([200, 0]),    2, '[200, 0] => 2');
    deepEqual(g.positionToIndex([10, 101]),   3, '[10, 101] => 3');
    deepEqual(g.positionToIndex([100, 100]),  4, '[100, 100] => 4');
    deepEqual(g.positionToIndex([200, 100]),  5, '[200, 100] => 5');
    deepEqual(g.positionToIndex([0, 200]),    6, '[0, 200] => 6');
    deepEqual(g.positionToIndex([100, 200]),  7, '[100, 200] => 7');
    deepEqual(g.positionToIndex([200, 200]),  8, '[200, 200] => 8');

    throws(function(){
      g.positionToIndex(1);
    }, 'Throws an error if the position param is not an array');
  });


  module('Insertion');

  test('#insert - An arbitrary value can be inserted into the GridPartition, by cell index', function(){
    var g = new GridPartition([300, 300], [100, 100]);
    g.insert(1, 'Test');
    ok(g._map[1][0]     == 'Test', 'An object is inserted into the map at index 1');
    ok(g._residents[1]  == 1,      'Map residents is increased by 1');
    ok(g.length         == 1,      'Increments it`s length property');
  });


  module('Removal');

  test('#remove - A value can be removed from the GridPartition', function(){
    var g = new GridPartition([300, 300], [100, 100]);
    g.insert(1, 'Test');
    g.remove(1, 'Test');
    ok(g._map[1][0]     == null, 'An object is removed from index 1');
    ok(g._residents[1]  == null, 'Map residents is decreased by 1, and set to null if at 0');
    ok(g.length         == 0,    'Decrements it`s length property');
  });


  module('Getting Cell Contents');

  test('#getCell - Get the contents of a particular cell by it`s index', function(){
    var g = new GridPartition([300, 300], [100, 100]);
    g.insert(5, 'One');
    g.insert(6, 'Two');
    g.insert(6, 'Three');
    deepEqual(g.getCell(5), ['One'],           '5 has one element');
    deepEqual(g.getCell(6), ['Two', 'Three'],  '6 has two elements');
    deepEqual(g.getCell(1), [],                '1 is empty');
  });

  test('#getCells - Gets the contents of multiple cells', function(){
    var g = new GridPartition([300, 300], [100, 100]);
    g.insert(5, 'One');
    g.insert(6, 'Two');
    g.insert(6, 'Three');
    deepEqual(g.getCells([5, 6, 7]), ['One', 'Two', 'Three'], '5 and 6 have three element');
  });

  test('#getAdjacentIndices - Get the indices of all cells surrounding a target cell', function(){
    var g = new GridPartition([300, 300], [100, 100]);

    // Center
    deepEqual(g.getAdjacentIndices(4).sort(), [0, 1, 2, 3, 5, 6, 7, 8], 'Retrieves the indices of all surrounding cells');

    // Corners
    deepEqual(g.getAdjacentIndices(0).sort(), [1, 3, 4], 'Index 0 - top left corner cells');
    deepEqual(g.getAdjacentIndices(2).sort(), [1, 4, 5], 'Index 2 - top right corner cells');
    deepEqual(g.getAdjacentIndices(6).sort(), [3, 4, 7], 'Index 6 - bottom left corner cells');
    deepEqual(g.getAdjacentIndices(8).sort(), [4, 5, 7], 'Index 8 - bottom right corner cells');

    // Edges
    deepEqual(g.getAdjacentIndices(1).sort(), [0, 2, 3, 4, 5], 'Index 1 - top');
    deepEqual(g.getAdjacentIndices(3).sort(), [0, 1, 4, 6, 7], 'Index 3 - left');
    deepEqual(g.getAdjacentIndices(5).sort(), [1, 2, 4, 7, 8], 'Index 5 - right');
    deepEqual(g.getAdjacentIndices(7).sort(), [3, 4, 5, 6, 8], 'Index 7 - bottom');
  });

  test('#getLinearIndices - Get the indices of cells in a line from a starting index', function(){
    var g = new GridPartition([500, 500], [100, 100]);

    deepEqual(g.getLinearIndices(0,   [1, 0]),  [1, 2, 3, 4],     'Right from 0 are indices: 1, 2, 3, 4');
    deepEqual(g.getLinearIndices(5,   [1, 0]),  [6, 7, 8, 9],     'Right from 5 are indices: 6, 7, 8, 9');
    deepEqual(g.getLinearIndices(20,  [1, 0]),  [21, 22, 23, 24], 'Right from 20 are indices: 21, 22, 23, 24');

    deepEqual(g.getLinearIndices(4,   [-1, 0]),  [3, 2, 1, 0],     'Left from 4 are indices: 3, 2, 1, 0');
    deepEqual(g.getLinearIndices(9,   [-1, 0]),  [8, 7, 6, 5],     'Left from 9 are indices: 8, 7, 6, 5');
    deepEqual(g.getLinearIndices(24,  [-1, 0]),  [23, 22, 21, 20], 'Left from 24 are indices: 23, 22, 21, 20');

    deepEqual(g.getLinearIndices(0, [0, 1]), [5, 10, 15, 20], 'Down from 0 are indices: 5, 10, 15, 20');
    deepEqual(g.getLinearIndices(1, [0, 1]), [6, 11, 16, 21], 'Down from 1 are indices: 6, 11, 16, 21');
    deepEqual(g.getLinearIndices(4, [0, 1]), [9, 14, 19, 24], 'Down from 4 are indices: 9, 14, 19, 24');

    deepEqual(g.getLinearIndices(20, [0, -1]), [15, 10, 5, 0], 'Up from 20 are indices: 15, 10, 5, 0');
    deepEqual(g.getLinearIndices(21, [0, -1]), [16, 11, 6, 1], 'Up from 21 are indices: 16, 11, 6, 1');
    deepEqual(g.getLinearIndices(24, [0, -1]), [19, 14, 9, 4], 'Up from 24 are indices: 19, 14, 9, 4');

    deepEqual(g.getLinearIndices(0,   [1, 1]),    [6, 12, 18, 24], 'Diagonal from 0 are indices: 6, 12, 18, 24');
    deepEqual(g.getLinearIndices(4,   [-1, 1]),   [8, 12, 16, 20], 'Diagonal from 4 are indices: 8, 12, 16, 2');
    deepEqual(g.getLinearIndices(20,  [1, -1]),   [16, 12, 8, 4],  'Diagonal from 20 are indices: 16, 12, 8, 4');
    deepEqual(g.getLinearIndices(24,  [-1, -1]),  [18, 12, 6, 0],  'Diagonal from 24 are indices: 16, 12, 8, 4');

    deepEqual(g.getLinearIndices(0,   [1, 1], 2),    [6, 12],  'Diagonal, limit 2, from 0 are indices: 6, 12');
    deepEqual(g.getLinearIndices(4,   [-1, 1], 2),   [8, 12],  'Diagonal, limit 2, from 4 are indices: 6, 12');
    deepEqual(g.getLinearIndices(20,  [1, -1], 2),   [16, 12], 'Diagonal, limit 2, from 20 are indices: 16, 12');
    deepEqual(g.getLinearIndices(24,  [-1, -1], 2),  [18, 12], 'Diagonal, limit 2, from 24 are indices: 16, 12');

    deepEqual(g.getLinearIndices(15,  [1, 1], 10),   [21],   'Diagonal, limit 10, from 15 are indices: 21');
    deepEqual(g.getLinearIndices(19,  [-1, 1], 10),  [23],   'Diagonal, limit 10, from 19 are indices: 23');
    deepEqual(g.getLinearIndices(10,  [1, -1], 10),  [6, 2], 'Diagonal, limit 10, from 10 are indices: 6, 2');
    deepEqual(g.getLinearIndices(7,   [-1, -1], 10), [1],    'Diagonal, limit 10, from 7 are indices: 1');
  });

  test('#getAdjacentCells - Get the contents of all cells surrounding a target cell', function(){
    var g = new GridPartition([3, 3], [1, 1]);
    g.insert(0, 'Zero');
    g.insert(1, 'One');
    g.insert(2, 'Two');
    g.insert(3, 'Three');
    g.insert(4, 'Four');
    g.insert(4, 'Four 2');
    g.insert(5, 'Five');
    g.insert(6, 'Six');
    g.insert(7, 'Seven');
    g.insert(8, 'Eight');

    // Centered
    deepEqual(g.getAdjacentCells(4).sort(),      ['Zero', 'One', 'Two', 'Three', 'Five', 'Six', 'Seven', 'Eight'].sort(), 'Retrieves the contents of surrounding cells, by index');

    // Top left corner
    deepEqual(g.getAdjacentCells(0).sort(),      ['One', 'Three', 'Four', 'Four 2'].sort(), 'Retrieves the contents of surrounding cells, by index');

    // Top Right corner
    deepEqual(g.getAdjacentCells(2).sort(),      ['One', 'Four', 'Four 2', 'Five'].sort(), 'Retrieves the contents of surrounding cells, by index');

    // Bottom left corner
    deepEqual(g.getAdjacentCells(6).sort(),      ['Three', 'Four', 'Four 2', 'Seven'].sort(), 'Retrieves the contents of surrounding cells, by index');

    // Bottom right corner
    deepEqual(g.getAdjacentCells(8).sort(),      ['Four', 'Four 2', 'Five', 'Seven'].sort(), 'Retrieves the contents of surrounding cells, by index');
  });


  module('Querying');

  test('#getResidents - Returns a map of the of the GridPartition population', function(){
    var g = new GridPartition([3, 3], [1, 1]);
    g.insert(0, 'One');
    g.insert(8, 'Two');
    g.insert(8, 'Three');
    deepEqual(g.getResidents(), {0: 1, 8: 2}, 'Residents is a hash of cell indexes and counts of objects in that cell');
  });

})();
